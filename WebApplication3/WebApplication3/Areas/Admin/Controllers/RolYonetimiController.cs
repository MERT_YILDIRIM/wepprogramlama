﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication3.Areas.Admin.Models;
using WebApplication3.Models;

namespace WebApplication3.Areas.Admin.Controllers
{
    [Authorize(Roles = "yonetici")]
    public class RolYonetimiController : Controller
    {

        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetimi
        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(roleStore);
            var model = rolManager.Roles.ToList();

            return View(model);
        }
        public ActionResult RoleEkle()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult RoleEkle(RoleEkleModel rol)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(roleStore);

            if (rolManager.RoleExists(rol.RolAd)==false)
            {
                rolManager.Create(new IdentityRole(rol.RolAd));
            }
            return RedirectToAction("Index");
        }

        public ActionResult RoleKullaniciEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolKullaniciEkle(RoleKullaniciEkleModel model)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);


            var kullanici = userManager.FindByName(model.KullaniciAdi);

            if (!userManager.IsInRole(kullanici.Id, model.RolAdi))
            {
                userManager.AddToRole(kullanici.Id, model.RolAdi);
            }

            return RedirectToAction("Index");
        }
    }
}
