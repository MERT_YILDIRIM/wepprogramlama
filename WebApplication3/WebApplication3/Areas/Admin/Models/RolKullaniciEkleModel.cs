﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Areas.Admin.Models
{
    public class RoleKullaniciEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}